package cellular;
import java.util.Random;


import datastructure.CellGrid;
import datastructure.IGrid;


public class BriansBrain implements CellAutomaton{

	
	IGrid currentGeneration;
	
	
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
    }
    

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }
    
	    

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		CellState state = currentGeneration.get(row, col);
		return state;
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int row = 0; row < numberOfRows(); row++) {
            for (int col = 0; col < numberOfColumns(); col++) {
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
		currentGeneration = nextGeneration;
	}		

	@Override
	public CellState getNextCell(int row, int col) {
        CellState currentCell = currentGeneration.get(row, col);
        int living_neighbors = countNeighbors(row, col, CellState.ALIVE);

        if (currentCell == CellState.ALIVE) {
            return CellState.DYING;
        } 
        else if (currentCell == CellState.DYING) {
            return CellState.DEAD;
        } 
        else if (currentCell == CellState.DEAD && living_neighbors == 2){
            return CellState.ALIVE;
        }
        else {
           return CellState.DEAD; 
        }
        
        
    }

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {

		if (row < 0 || row >= numberOfRows() || col < 0 || col > numberOfColumns()) {
            throw new IndexOutOfBoundsException("nei nei index ikke med");
        }
		int counter = 0;
		for (int i=row-1; i<row+2; i++){
			for (int n = col-1; n<col+2; n++){
				if ( i<0 || n<0 ){
					continue;
				}
				if (i >= currentGeneration.numRows() || n >= currentGeneration.numColumns()){
					continue;
				}
				if (i == row && n == col){
					continue;
				}
				else if (currentGeneration.get(i,n).equals(state)){
					counter ++;
				
				}

			}
		}
		return counter;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}

}
